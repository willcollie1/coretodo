/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import com.celestial.dsb.core.URLManipulator;
import com.celestial.dsb.core.IURLShortener;
import java.util.Arrays;
import java.util.Collection;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Selvyn
 */
@RunWith(Parameterized.class)
public class URLManipulatorTest
{
    private URLManipulator  urlManipulator;
    private String  longUrl;
    
    public  URLManipulatorTest( String url )
    {
        this.longUrl = url;
    }
    
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"www.google.com/"}, 
            {"www.google.com"},
            {"http://www.yahoo.com"}, 
            {"www.yahoo.com/"}, 
            {"www.amazon.com"},
            {"www.amazon.com/page1.php"}, 
            {"www.amazon.com/page2.php"},
            {"www.flipkart.in"}, 
            {"www.rediff.com"}, 
            {"www.techmeme.com"},
            {"www.techcrunch.com"}, 
            {"www.lifehacker.com"}, 
            {"www.icicibank.com"},
            {"https://celestialconsulting.atlassian.net/wiki/spaces/MSD/pages/109851535/Docker+stuff"}
        });
    }

    @Before
    public void setUp()
    {
        urlManipulator = new URLManipulator();
    }
    
    @Test
    public  void    create_shortened_url()
    {
        String input = longUrl;
        String shortUrl = urlManipulator.shortenURL(input);
        String originalUrl = urlManipulator.expandShortenedURL(shortUrl);
        IURLShortener shortener = urlManipulator.getURLShortener();

        assertThat( originalUrl, is( shortener.sanitizeURL( input )));
    }
}
